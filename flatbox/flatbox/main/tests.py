# coding: utf-8

import os
import hashlib
import shutil

from django.test import TestCase
from django.test.utils import override_settings
from django.contrib.auth.models import User

from flatbox.main.uploadhandler import HashingUploadedFile
from flatbox.main.uploadhandler import HashingUploadHandler
from flatbox.main import models
from flatbox.main import views
from flatbox.main import storage


DIR = os.path.dirname(__file__)
TEST_DIR = os.path.join(DIR, "fixtures", "test_upload")
FIXT_DIR = os.path.join(DIR, "fixtures")


def fopen(fname):
    return open(os.path.join(FIXT_DIR, fname))


class HashingUploadedFileTest(TestCase):

    def test_write(self):
        uf = HashingUploadedFile("name", "content_type", 100, "charset")
        uf.write("part_1")
        uf.write("part_2")
        uf.file.seek(0)
        self.assertEquals(uf.file.read(), "part_1part_2")

    def test_hash(self):
        uf = HashingUploadedFile("name", "content_type", 100, "charset")
        uf.write("part_1")
        uf.write("part_2")
        self.assertEquals(uf.hash, hashlib.sha256("part_1part_2").hexdigest())
        uf.write("part_3")
        self.assertEquals(uf.hash,
                          hashlib.sha256("part_1part_2part_3").hexdigest())


class BaseTest(TestCase):

    fixtures = ["full.json"]

    def setUp(self):
        if os.path.exists(TEST_DIR):
            shutil.rmtree(TEST_DIR)

        os.mkdir(TEST_DIR)
        shutil.copy(
            os.path.join(FIXT_DIR, "1.txt"),
            os.path.join(TEST_DIR, "1.txt"))
        shutil.copy(
            os.path.join(FIXT_DIR, "2.jpg"),
            os.path.join(TEST_DIR, "2.jpg"))
        self.user_1 = models.User.objects.get(pk=1)

    def mk_user(self, n):
        user = User.objects.create_user(
            username='user_{}'.format(n),
            email='user@mail.com',
            password='password_{}'.format(n))
        return user

    def tearDown(self):
        if os.path.exists(TEST_DIR):
            shutil.rmtree(TEST_DIR)


class ViewsTest(BaseTest):

    @override_settings(MEDIA_ROOT=TEST_DIR)
    def test_upload_new(self):
        self.client.login(username="user_1", password="password_1")
        response = self.client.post('/upload/', {"file": fopen("3.txt")})
        self.assertRedirects(response, '/')
        stored_files = models.StoredFile.objects.all()
        user_files = models.UserFile.objects.all()
        self.assertEquals(len(user_files), 3)
        self.assertEquals(len(stored_files), 3)
        self.assertEquals(user_files[2].filename, "3.txt")
        self.assertEquals(user_files[2].user, self.user_1)
        self.assertEquals(user_files[2].content_type, "text/plain")
        self.assertEquals(user_files[2].stored_file, stored_files[2])
        self.assertEquals(stored_files[2].original_username, "user_1")

        response = self.client.get('/')
        self.assertContains(response, "3.txt")

    @override_settings(MEDIA_ROOT=TEST_DIR)
    def test_upload_exist(self):
        u2 = self.mk_user(2)
        self.client.login(username="user_2", password="password_2")

        response = self.client.post('/upload/', {"file": fopen("2.jpg")})
        self.assertRedirects(response, '/')
        stored_files = models.StoredFile.objects.all()
        user_files = models.UserFile.objects.all()
        own_files = models.UserFile.objects.filter(user=u2).all()
        self.assertEquals(len(stored_files), 2)
        self.assertEquals(len(user_files), 3)
        self.assertEquals(len(own_files), 1)
        self.assertEquals(own_files[0].filename, "2.jpg")
        self.assertEquals(own_files[0].user, u2)
        self.assertEquals(own_files[0].content_type, "image/jpeg")
        self.assertEquals(own_files[0].stored_file, stored_files[1])

        response = self.client.get('/')
        self.assertContains(response, "2.jpg")

    @override_settings(MEDIA_ROOT=TEST_DIR)
    def test_delete(self):
        self.client.login(username="user_1", password="password_1")

        response = self.client.post('/delete/1/')
        self.assertRedirects(response, '/')
        self.assertEquals(modesl.UserFile.objecs.count(), 1)
        self.assertEquals(modesl.StoredFile.objecs.count(), 2)

        response = self.client.post('/delete/2/')
        self.assertRedirects(response, '/')
        self.assertEquals(modesl.UserFile.objecs.count(), 1)
        self.assertEquals(modesl.StoredFile.objecs.count(), 2)

        response = self.client.post('/delete/555/')
        self.assertRedirects(response, '/')

    @override_settings(MEDIA_ROOT=TEST_DIR)
    def test_delete(self):
        self.client.login(username="user_1", password="password_1")
        response = self.client.post('/delete/1/', {})
        self.assertRedirects(response, '/')

    @override_settings(MEDIA_ROOT=TEST_DIR)
    def test_get(self):
        response = self.client.get('/get/1/1.txt')
        self.assertEquals(response.streaming_content.next(), "file_one")
        self.assertEquals(response['content-disposition'], "attachment;")
        self.assertEquals(response['content-type'], "text/plain")

        response = self.client.get('/get/2/2.jpg')
        self.assertEquals(response.streaming_content.next(), "file_two")
        self.assertEquals(response['content-disposition'], "attachment;")
        self.assertEquals(response['content-type'], "image/jpeg")


class StorageTest(BaseTest):

    def test_delete_unused_files(self):
        self.assertEquals(models.StoredFile.objects.count(), 2)
        storage.Storage.remove_unused_files()
        self.assertEquals(models.StoredFile.objects.count(), 2)

        s = storage.Storage(self.user_1)
        s.delete_file(1)
        s.delete_file(2)

        self.assertEquals(models.StoredFile.objects.count(), 2)
        storage.Storage.remove_unused_files()
        self.assertEquals(models.StoredFile.objects.count(), 0)
