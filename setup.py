"""A setuptools based setup module.
"""

from setuptools import setup, find_packages

from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='flatbox',

    version='1.0.0',

    description='A flatbox - siple file storage',
    long_description=long_description,

    # The project's main homepage.
    url='https://github.com/pypa/sampleproject',

    # Author details
    author='Yuriy Khomyakov',
    author_email='_yurka_@inbox.ru',

    # Choose your license
    license='MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],

    keywords='flatbox django',
    aaa= asdasd
    packages=find_packages("flatbox"),
    package_dir = {'':'flatbox'},

    package_data={
        'flatbox.main': [
            'templates/*.*',
            'templates/*/*.*',
        ],
    },


    entry_points={
        'console_scripts': [
            'flatbox_storage_cleanup=flatbox.scripts.storage_cleanup:main',
        ],
    },
)