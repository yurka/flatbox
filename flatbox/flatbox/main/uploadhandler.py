# coding: utf-8

import hashlib

from django.core.files.uploadhandler import TemporaryFileUploadHandler
from django.core.files.uploadedfile import TemporaryUploadedFile


class HashingUploadedFile(TemporaryUploadedFile):

    def __init__(self, *args, **kw):
        super(HashingUploadedFile, self).__init__(*args, **kw)
        self._hash_func = hashlib.sha256()

    def write(self, raw_data):
        super(HashingUploadedFile, self).write(raw_data)
        self._update_hash(raw_data)

    def _update_hash(self, raw_data):
        self._hash_func.update(raw_data)

    @property
    def hash(self):
        return self._hash_func.hexdigest()


class HashingUploadHandler(TemporaryFileUploadHandler):

    def new_file(self, file_name, *args, **kwargs):
        """
        Create the file object to append to as data is coming in.
        """
        super(HashingUploadHandler, self).new_file(file_name, *args, **kwargs)
        self.file = HashingUploadedFile(self.file_name, self.content_type, 0,
                                        self.charset, self.content_type_extra)
