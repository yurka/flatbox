# coding: utf-8

from datetime import datetime

from django.http import (HttpResponse, HttpResponseRedirect,
                         Http404, StreamingHttpResponse)
from django.shortcuts import (render_to_response, get_object_or_404,
                              render, redirect)
from django.template import RequestContext
from django.db import connection, transaction
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import auth
from django.contrib import messages


from flatbox.main.storage import Storage
from flatbox.main import models

EXIST_MSG = (u"Загружаемый файл является дублем."
             u" Пользователь {} уже загрузил такой файл "
             u"в систему под именем {}")


def sign_up(request):
    """
    Регистрация пользователя
    """
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            return HttpResponseRedirect("/")
    else:
        form = UserCreationForm()
    return render_to_response(
        "registration/sign_up.html",
        locals(),
        context_instance=RequestContext(request))


@login_required
def files(request):
    """
    Файлы пользователя
    """
    storage = Storage(request.user)
    files_list = storage.list_files()
    return render_to_response(
        "files.html",
        locals(),
        context_instance=RequestContext(request))


@login_required
def upload_file(request):
    """
    Загрузка файла
    """
    storage = Storage(request.user)
    file_ = request.FILES.get('file')
    if file_:
        user_file, original = storage.add_file(file_)
        if original is not None:
            msg = EXIST_MSG.format(
                original.original_username, original.original_name)
            messages.info(request, msg)
    return HttpResponseRedirect('/')


@login_required
def delete_file(request, file_id):
    """
    удаление файла
    """
    storage = Storage(request.user)
    storage.delete_file(file_id)
    return HttpResponseRedirect('/')

import urllib


def download_file(request, pk, filename):
    """
    Скачивание файла
    """
    user_file = Storage.get_user_file(pk)
    if user_file is None:
        raise Http404
    response = StreamingHttpResponse(user_file.fileo,
                                     content_type=user_file.content_type)
    response['Content-Disposition'] = u'attachment;'
    return response
