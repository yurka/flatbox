# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='StoredFile',
            fields=[
                ('file_hash', models.CharField(max_length=64, serialize=False, verbose_name='\u0425\u044d\u0448', primary_key=True)),
                ('original_name', models.CharField(max_length=1000, verbose_name='\u0418\u043c\u044f \u043e\u0440\u0438\u0433\u0438\u043d\u0430\u043b\u044c\u043d\u043e\u0433\u043e \u0444\u0430\u0439\u043b\u0430')),
                ('original_username', models.CharField(max_length=1000, verbose_name='\u0418\u043c\u044f \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f, \u0434\u043e\u0431\u0430\u0432\u0438\u0432\u0448\u0435\u0433\u043e \u0444\u0430\u0439\u043b \u0432 \u0441\u0438\u0441\u0442\u0435\u043c\u0443')),
                ('file', models.FileField(upload_to=b'', max_length=1000, verbose_name='\u0424\u0430\u0439\u043b')),
            ],
        ),
        migrations.CreateModel(
            name='UserFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('filename', models.CharField(max_length=1000, verbose_name='\u0418\u043c\u044f \u0444\u0430\u0439\u043b\u0430')),
                ('content_type', models.CharField(max_length=100)),
                ('stored_file', models.ForeignKey(related_name='user_files', on_delete=django.db.models.deletion.PROTECT, to='main.StoredFile')),
                ('user', models.ForeignKey(related_name='files', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
