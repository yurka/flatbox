# coding: utf-8

import logging
import time

from django.db import transaction
from django.db.models.deletion import ProtectedError
from django.db.utils import IntegrityError
from flatbox.main import models

logger = logging.getLogger("flatbox")

QUOTA = 100


class Storage(object):

    """
    Файловое хранилище
    """

    def __init__(self, user):
        self.user = user

    @transaction.atomic
    def add_file(self, file):
        """
        Добавить файл
        """
        logger.info("Adding userfile, user %s, filename %s, hash %s",
                    self.user.username, file.name, file.hash)

        assert self.get_limits > 0, "quota exceeded"

        original = None

        stored_file = self._get_stored_file(file.hash)
        if stored_file is None:
            stored_file = self._save_file(file)
        else:
            original = stored_file

        user_file = models.UserFile(
            filename=file.name,
            user=self.user,
            stored_file=stored_file,
            content_type=file.content_type)
        user_file.save()

        logger.info("Userfile #%s added, user %s, filename %s, hash %s",
                    user_file.pk, self.user.username, file.name, file.hash)

        return user_file, original

    def delete_file(self, file_id):
        """
        Удалить файл
        """
        logger.info("Deleting userfile #%s, user %s",
                    file_id, self.user.username)

        models.UserFile.objects.filter(user=self.user, pk=file_id).delete()

    def list_files(self):
        """
        Получить список файлов ползователя
        """
        query = models.UserFile.objects.filter(user=self.user)
        return query.all()

    @staticmethod
    def get_user_file(pk):
        """
        Получить файл пользователя по ключу
        """
        try:
            return models.UserFile.objects.get(pk=pk)
        except models.UserFile.DoesNotExist:
            return None

    def get_limits(self):
        """
        Сколько еще файлов пользователь может загрузить в хранилище
        """
        query = models.UserFile.objects.filter(user=self.user)
        return QUOTA - query.count()

    @staticmethod
    def remove_unused_files():
        """
        Удаляет все неиспользуемые файлы из хранилища
        """
        logger.info("Deleting unused files")
        query = models.StoredFile.objects.filter(user_files=None)
        cnt = 0
        for obj in query.all():
            try:
                obj.delete()
                obj.file.delete(False)
                cnt += 1
            except (ProtectedError, IntegrityError):
                logger.info("Can not delete %s", obj.file_hash)
                pass
        logger.info("%s unused files deleted", cnt)

    def _get_stored_file(self, hash):
        """
        Проверить что такой файл уже существует
        """
        try:
            return models.StoredFile.objects.select_for_update().get(pk=hash)
        except models.StoredFile.DoesNotExist:
            return None

    def _save_file(self, file):
        """
        Сохраняет новый файл в хранилище
        """
        logger.info("Saving new file, user %s, filename %s, hash %s",
                    self.user.username, file.name, file.hash)

        new_file = models.StoredFile.objects.create(
            file_hash=file.hash,
            file=file,
            original_name=file.name,
            original_username=self.user.username)

        logger.info("New file saved, user %s, filename %s, hash %s",
                    self.user.username, file.name, file.hash)
        return new_file
