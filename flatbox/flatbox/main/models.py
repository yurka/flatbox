# coding: utf-8

from django.db import models
from django.contrib.auth.models import User


class StoredFile(models.Model):

    """
    Файлы, хранящиеся в системе
    """
    class Meta:
        ordering = ["original_name"]

    file_hash = models.CharField(
        verbose_name=u"Хэш",
        max_length=64,
        primary_key=True,
        null=False,
        blank=False)

    original_name = models.CharField(
        verbose_name=u"Имя оригинального файла",
        max_length=1000)

    original_username = models.CharField(
        verbose_name=u"Имя пользователя, добавившего файл в систему",
        max_length=1000)

    file = models.FileField(
        verbose_name=u"Файл",
        max_length=1000)


class UserFile(models.Model):

    """
    Файлы пользователя
    """

    class Meta:
        ordering = ["filename", "pk"]

    filename = models.CharField(
        verbose_name=u"Имя файла",
        max_length=1000)

    stored_file = models.ForeignKey(
        StoredFile,
        related_name="user_files",
        on_delete=models.PROTECT)

    user = models.ForeignKey(User, related_name="files")
    content_type = models.CharField(max_length=100)

    @property
    def fileo(self):
        return self.stored_file.file
