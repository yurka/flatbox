#!/usr/bin/env python
import os
import sys


def main():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "flatbox.settings")
    from django.core.management import execute_from_command_line
    from django.core.management import call_command
    call_command("storage_cleanup")

    
if __name__ == "__main__":
    main()