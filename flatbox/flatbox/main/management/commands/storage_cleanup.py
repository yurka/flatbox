# coding: utf-8

import logging

from django.core.management.base import BaseCommand, CommandError

from flatbox.main.storage import Storage


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "cleanups storage - removes unused files."

    requires_system_checks = False

    def handle(self, *app_labels, **options):
        try:
            Storage.remove_unused_files()
        except Exception, e:
            logger.error("Storage cleanup failed")
            raise
