from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'flatbox.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url('^', include('django.contrib.auth.urls')),


    url(r'^$', "flatbox.main.views.files"),
    # url(r'^login/$', "flatbox.main.views.login_screen"),
    # url(r'^sign_in/$', "flatbox.main.views.sign_in"),
    url(r'^sign_up/$', "flatbox.main.views.sign_up"),
    url(r'^upload/$', "flatbox.main.views.upload_file"),
    url(r'^delete/(?P<file_id>\d+)/$', "flatbox.main.views.delete_file"),
    url(r'^get/(?P<pk>\d+)/(?P<filename>.+)$', "flatbox.main.views.download_file"),
)
